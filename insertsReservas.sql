INSERT INTO reservas(
    reserva_uuid, id_reserva, 
    usr_casero_uuid, 
    usr_inquilino_uuid,
    inmueble_uuid, 
    anuncio_uuid, 
    fecha_reserva, fecha_inicio, fecha_fin, precio_reserva, estado_reserva, tipo_pago_reserva)
VALUES(
    "0ee793e5-f193-41a7-ac09-05aa73aef23c", null, 
    "3983bb27-f996-48a5-9cff-22c14ce6421f",
    "a32f5c15-f7fd-44c8-9476-efcbf26a628c",
    "0eff0364-291d-4001-b3cc-321725d7cd27",
    "5f6bc9e7-9317-40d0-96a7-26681423b85f", CURDATE(), CURDATE(), CURDATE(), 0, 'ALQUILER', 'MENSUAL'
);

INSERT INTO reservas(
    reserva_uuid, id_reserva, 
    usr_casero_uuid, 
    usr_inquilino_uuid,
    inmueble_uuid, 
    anuncio_uuid, 
    fecha_reserva, fecha_inicio, fecha_fin, precio_reserva, estado_reserva, tipo_pago_reserva)
VALUES(
    "de3c3bfa-3fcc-4248-818b-2a7f84acc7e7", null, "3983bb27-f996-48a5-9cff-22c14ce6421f",
    "b0cd1ee7-3979-4877-89a7-19dd4585335d","4929f456-7039-4dee-b2ad-39d8fa92a675",
    "ffdbd3c9-13ff-4e47-bca3-85d92dc04d4a", CURDATE(), CURDATE(), CURDATE(), 0, 'PENDIENTE', 'MENSUAL'
);

INSERT INTO reservas(
    reserva_uuid, id_reserva, 
    usr_casero_uuid, 
    usr_inquilino_uuid,
    inmueble_uuid, 
    anuncio_uuid, 
    fecha_reserva, fecha_inicio, fecha_fin, precio_reserva, estado_reserva, tipo_pago_reserva)
VALUES(
    "ead7c19e-1742-4de8-aa59-393c35328d99", null, "d0f3bb42-fa87-4e3a-89d3-eb608eebc26b",
    "b0cd1ee7-3979-4877-89a7-19dd4585335d","90beefa2-9d36-4c02-889b-9df2414439de",
    "af052ba5-7383-4e63-81e2-6a1416647149", CURDATE(), CURDATE(), CURDATE(), 0, 'RECHAZADO', 'MENSUAL'
);

INSERT INTO reservas(
    reserva_uuid, id_reserva, 
    usr_casero_uuid, 
    usr_inquilino_uuid,
    inmueble_uuid, 
    anuncio_uuid, 
    fecha_reserva, fecha_inicio, fecha_fin, precio_reserva, estado_reserva, tipo_pago_reserva)
VALUES(
    "067b6c2b-e8ea-404a-9395-e15a7b5eab74", null, "d0f3bb42-fa87-4e3a-89d3-eb608eebc26b",
    "b0cd1ee7-3979-4877-89a7-19dd4585335d","90beefa2-9d36-4c02-889b-9df2414439de",
    "af052ba5-7383-4e63-81e2-6a1416647149", CURDATE(), CURDATE(), CURDATE(), 0, 'RECHAZADO', 'MENSUAL'
);

INSERT INTO reservas(
    reserva_uuid, id_reserva, 
    usr_casero_uuid, 
    usr_inquilino_uuid,
    inmueble_uuid, 
    anuncio_uuid, 
    fecha_reserva, fecha_inicio, fecha_fin, precio_reserva, estado_reserva, tipo_pago_reserva)
VALUES(
    "fec8d109-2411-4b76-9190-2e24b1e0d7a1", null, "d0f3bb42-fa87-4e3a-89d3-eb608eebc26b",
    "b0cd1ee7-3979-4877-89a7-19dd4585335d","90beefa2-9d36-4c02-889b-9df2414439de",
    "af052ba5-7383-4e63-81e2-6a1416647149", CURDATE(), CURDATE(), CURDATE(), 0, 'RECHAZADO', 'MENSUAL'
);
