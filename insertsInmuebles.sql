INSERT INTO inmuebles
(inmueble_uuid, usr_casero_uuid, disponibilidad, metros_2, calle, numero, piso, ciudad, provincia, comunidad, pais, cp)
VALUES
	('90beefa2-9d36-4c02-889b-9df2414439de',
	'd0f3bb42-fa87-4e3a-89d3-eb608eebc26b', 
	1,
	90, 
	'Gran Vía', 
	'128', 
	'1º',
	'Vigo',
	'Pontevedra',
	'Galicia',
	'España',
	'36204'
);

INSERT INTO inmuebles
(inmueble_uuid, usr_casero_uuid, disponibilidad, metros_2, calle, numero, piso, ciudad, provincia, comunidad, pais, cp)
    VALUES(
	'4929f456-7039-4dee-b2ad-39d8fa92a675',
	'd0f3bb42-fa87-4e3a-89d3-eb608eebc26b',
	1,
	120,
	'Gran Vía',
	'134',
	'2º',
	'Vigo',
	'Pontevedra',
	'Galicia',
	'España',
	'36204'
);

INSERT INTO inmuebles
(inmueble_uuid, usr_casero_uuid, disponibilidad, metros_2, calle, numero, piso, ciudad, provincia, comunidad, pais, cp)
    VALUES(
	'a38e5b1e-e809-492a-b183-836d17143b22',
	'd0f3bb42-fa87-4e3a-89d3-eb608eebc26b',
	true,
	90,
	'Camelias',
	'134',
	'3ºA',
	'Vigo',
	'Pontevedra',
	'Galicia',
	'España',
	'36203'
);


INSERT INTO inmuebles
(inmueble_uuid, usr_casero_uuid, disponibilidad, metros_2, calle, numero, piso, ciudad, provincia, comunidad, pais, cp)
    VALUES(
	'bb3c4fcc-d17d-4341-a4b5-30b373e78f8a',
	'd0f3bb42-fa87-4e3a-89d3-eb608eebc26b',
	1,
	120,
	'Malasaña',
	'2',
	'BAJO',
	'Madrid',
	'Madrid',
	'Madrid',
	'España',
	'21808'
);

INSERT INTO inmuebles
(inmueble_uuid, usr_casero_uuid, disponibilidad, metros_2, calle, numero, piso, ciudad, provincia, comunidad, pais, cp)
    VALUES(
	'af593913-a5b2-4938-b14b-8073b873b5e8',
	'd0f3bb42-fa87-4e3a-89d3-eb608eebc26b',
	true,
	120,
	'Malasaña',
	'4',
	'BAJO',
	'Madrid',
	'Madrid',
	'Madrid',
	'España',
	'21808'
);

INSERT INTO inmuebles
(inmueble_uuid, usr_casero_uuid, disponibilidad, metros_2, calle, numero, piso, ciudad, provincia, comunidad, pais, cp)    
	VALUES(
	'e897672a-403e-4e28-90e9-1092467aa1e0',
	'3983bb27-f996-48a5-9cff-22c14ce6421f',
	true,
	90,
	'Malasaña',
	'2',
	'3º',
	'Madrid',
	'Madrid',
	'Madrid',
	'España',
	'21808'
);

INSERT INTO inmuebles
(inmueble_uuid, usr_casero_uuid, disponibilidad, metros_2, calle, numero, piso, ciudad, provincia, comunidad, pais, cp)    
    VALUES(
	'2fa0e1cd-3ea3-424a-84fb-6ad170ee94ce',
	'3983bb27-f996-48a5-9cff-22c14ce6421f',
	true,
	90,
	'Malasaña',
	'6º',
	'BAJO',
	'Madrid',
	'Madrid',
	'Madrid',
	'España',
	'21808'
);

INSERT INTO inmuebles
(inmueble_uuid, usr_casero_uuid, disponibilidad, metros_2, calle, numero, piso, ciudad, provincia, comunidad, pais, cp) 
    VALUES(
	'614d1447-8118-4a09-a774-de50838fcf91',
	'3983bb27-f996-48a5-9cff-22c14ce6421f',
	true,
	100,
	'Malasaña',
	'24',
	'BAJO',
	'Madrid',
	'Madrid',
	'Madrid',
	'España',
	'21808'
);

INSERT INTO inmuebles
(inmueble_uuid, usr_casero_uuid, disponibilidad, metros_2, calle, numero, piso, ciudad, provincia, comunidad, pais, cp) 
    VALUES(
	'0eff0364-291d-4001-b3cc-321725d7cd27',
	'3983bb27-f996-48a5-9cff-22c14ce6421f',
	true,
	90,
	'Malasaña',
	'7º',
	'BAJO',
	'Madrid',
	'Madrid',
	'Madrid',
	'España',
	'21808'
);

INSERT INTO inmuebles
(inmueble_uuid, usr_casero_uuid, disponibilidad, metros_2, calle, numero, piso, ciudad, provincia, comunidad, pais, cp) 
    VALUES(
	'7fc9832c-5c48-4a0b-9784-d7d69d4e8f61',
	'3983bb27-f996-48a5-9cff-22c14ce6421f',
	false,
	90,
	'Malasaña',
	'22',
	'BAJO',
	'Madrid',
	'Madrid',
	'Madrid',
	'España',
	'21808'
);

INSERT INTO inmuebles
(inmueble_uuid, usr_casero_uuid, disponibilidad, metros_2, calle, numero, piso, ciudad, provincia, comunidad, pais, cp) 
    VALUES(
	'c9345c00-d3fc-43d6-b149-8a44f7a691c7',
	'3983bb27-f996-48a5-9cff-22c14ce6421f',
	false,
	90,
	'Malasaña',
	'4',
	'BAJO',
	'Madrid',
	'Madrid',
	'Madrid',
	'España',
	'21808'
);

SELECT * FROM inmuebles;
