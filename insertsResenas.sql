--inquilino crear reserva mismo inquilino mismo casero
INSERT INTO resenas(
    resena_uuid, id_resena,
    reserva_uuid,
    autor_uuid,
    inmueble_uuid,
    usr_casero_uuid,
    usr_inquilino_uuid,
    puntuacion,
    contenido)
VALUES (
    "d6627575-237e-4c2b-acfa-e885492ef239", null,
    "067b6c2b-e8ea-404a-9395-e15a7b5eab74",
    "b0cd1ee7-3979-4877-89a7-19dd4585335d",
    "90beefa2-9d36-4c02-889b-9df2414439de",
    "d0f3bb42-fa87-4e3a-89d3-eb608eebc26b",
    "b0cd1ee7-3979-4877-89a7-19dd4585335d",
    5,
    "test55"
);

INSERT INTO resenas(
    resena_uuid, id_resena,
    reserva_uuid,
    autor_uuid,
    inmueble_uuid,
    usr_casero_uuid,
    usr_inquilino_uuid,
    puntuacion,
    contenido)
VALUES (
    "d6627575-237e-4c2b-acfa-e885492ef239", null,
    "fec8d109-2411-4b76-9190-2e24b1e0d7a1",
    "b0cd1ee7-3979-4877-89a7-19dd4585335d",
    "90beefa2-9d36-4c02-889b-9df2414439de",
    "d0f3bb42-fa87-4e3a-89d3-eb608eebc26b",
    "b0cd1ee7-3979-4877-89a7-19dd4585335d",
    5,
    "test55 parte 2"
);

INSERT INTO resenas(
    resena_uuid, id_resena,
    reserva_uuid,
    autor_uuid,
    inmueble_uuid,
    usr_casero_uuid,
    usr_inquilino_uuid,
    puntuacion,
    contenido)
VALUES (
    "089c123a-a632-463e-8682-2e4f5e9b882c", null,
    "0ee793e5-f193-41a7-ac09-05aa73aef23c",
    "a32f5c15-f7fd-44c8-9476-efcbf26a628c",
    "0eff0364-291d-4001-b3cc-321725d7cd27",
    "3983bb27-f996-48a5-9cff-22c14ce6421f",
    "a32f5c15-f7fd-44c8-9476-efcbf26a628c",
    5,
    "asdfghjklñsadfghjklñsdfghjklñdfghjk,.-"
);
--casero
INSERT INTO resenas(
    resena_uuid, id_resena,
    reserva_uuid,
    autor_uuid,
    inmueble_uuid,
    usr_casero_uuid,
    usr_inquilino_uuid,
    puntuacion,
    contenido)
VALUES (
    "5738ffe7-0da2-4efb-8ee9-8987993fdd55", null,
    "0ee793e5-f193-41a7-ac09-05aa73aef23c",
    "3983bb27-f996-48a5-9cff-22c14ce6421f",
    "0eff0364-291d-4001-b3cc-321725d7cd27",
    "3983bb27-f996-48a5-9cff-22c14ce6421f",
    "a32f5c15-f7fd-44c8-9476-efcbf26a628c",
    5,
    "poiuytrewqljhgfdsa,mn<bvcxz"
);

INSERT INTO resenas(
    resena_uuid, id_resena,
    reserva_uuid,
    autor_uuid,
    inmueble_uuid,
    usr_casero_uuid,
    usr_inquilino_uuid,
    puntuacion,
    contenido)
VALUES (
    "dfd99cbb-eede-4088-b59b-d3dbde6e605a", null,
    "de3c3bfa-3fcc-4248-818b-2a7f84acc7e7",
    "3983bb27-f996-48a5-9cff-22c14ce6421f",
    "4929f456-7039-4dee-b2ad-39d8fa92a675",
    "3983bb27-f996-48a5-9cff-22c14ce6421f",
    "b0cd1ee7-3979-4877-89a7-19dd4585335d",
    1,
    "asdfghasdfgasdfgASDFG"
);

