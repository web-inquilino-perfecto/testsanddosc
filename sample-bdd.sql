DROP DATABASE inmoweb;
CREATE DATABASE IF NOT EXISTS inmoweb;
USE inmoweb;

CREATE TABLE usuarios (
    user_uuid VARCHAR(64) NOT NULL,
    id_usuario INT UNSIGNED NOT NULL AUTO_INCREMENT,
    username VARCHAR(64) NOT NULL,
    password VARCHAR(64) NOT NULL,
    email VARCHAR(256) NOT NULL,
    tipo ENUM('INQUILINO','CASERO', 'INQUILINO/CASERO', 'ADMIN') NOT NULL DEFAULT 'INQUILINO',
    activated_at TIMESTAMP DEFAULT NULL,
    activated_code VARCHAR(64) DEFAULT NULL,
    modified_at TIMESTAMP DEFAULT NULL,
    deleted_at TIMESTAMP DEFAULT NULL,
    avatar VARCHAR(128) DEFAULT NULL,
    puntuacion_media DECIMAL(3,2) NOT NULL DEFAULT 0,

    CONSTRAINT UNIQUE KEY UK_usuarios_username (username),
    CONSTRAINT UNIQUE KEY UK_usuarios_email (email),
    CONSTRAINT UNIQUE INDEX UI_usuarios (id_usuario),
    CONSTRAINT PK_usuarios PRIMARY KEY(user_uuid)
);


CREATE TABLE inmuebles (
    inmueble_uuid VARCHAR(64) NOT NULL,
    usr_casero_uuid VARCHAR(64) NOT NULL,
    id_inmueble INT UNSIGNED NOT NULL AUTO_INCREMENT,
    disponibilidad BOOLEAN NOT NULL DEFAULT TRUE,
    puntuacion_media DECIMAL(3,2) NOT NULL DEFAULT 0,    
    lng DECIMAL(9,6) NOT NULL DEFAULT 0,
    lat DECIMAL(9,6) NOT NULL DEFAULT 0,

    metros_2 SMALLINT DEFAULT 0,

    calle VARCHAR(256) NOT NULL,
    numero VARCHAR (16),
    piso VARCHAR (16),
    ciudad VARCHAR(128) NOT NULL,
    provincia VARCHAR(128)NOT NULL,
    comunidad VARCHAR(128),
    pais VARCHAR (128) NOT NULL,
    cp VARCHAR (5) NOT NULL,
    
    banos SMALLINT NOT NULL DEFAULT 0,
    habitaciones SMALLINT NOT NULL DEFAULT 0,
    amueblado BOOLEAN DEFAULT FALSE,
    calefaccion BOOLEAN DEFAULT FALSE,
    aire_acondicionado BOOLEAN DEFAULT FALSE,
    jardin BOOLEAN DEFAULT FALSE,
    terraza BOOLEAN DEFAULT FALSE,
    ascensor BOOLEAN DEFAULT FALSE,
    piscina BOOLEAN DEFAULT FALSE,

    CONSTRAINT UNIQUE INDEX UI_inmuebles (id_inmueble),
    CONSTRAINT FK_inmuebles_usr_casero_uuid FOREIGN KEY (usr_casero_uuid) REFERENCES usuarios(user_uuid) 
        ON DELETE CASCADE, 
    CONSTRAINT PK_imnuebles PRIMARY KEY(inmueble_uuid, usr_casero_uuid)
);


CREATE TABLE img_inmuebles (
    img_inmueble_uuid VARCHAR(64) NOT NULL,
    id_img_inmueble INT UNSIGNED NOT NULL AUTO_INCREMENT,
    inmueble_uuid VARCHAR(64),
    img_inmueble VARCHAR(512),

    CONSTRAINT UNIQUE INDEX UI_img_inmuebles (id_img_inmueble),
    CONSTRAINT FK_img_inmuebles_inmueble_uuid FOREIGN KEY (inmueble_uuid) REFERENCES inmuebles(inmueble_uuid)
        ON DELETE CASCADE,
    CONSTRAINT PK_img_inmuebles PRIMARY KEY (img_inmueble_uuid)
);

CREATE TABLE anuncios (
    anuncio_uuid VARCHAR(64) NOT NULL,
    id_anuncio INT UNSIGNED NOT NULL AUTO_INCREMENT,
    inmueble_uuid VARCHAR(64),
    usr_casero_uuid VARCHAR(64),
    fecha_disponibilidad DATE NOT NULL,
    visibilidad BOOLEAN NOT NULL DEFAULT TRUE,
    precio SMALLINT NOT NULL DEFAULT 100,

    CONSTRAINT UNIQUE INDEX UI_anuncios (id_anuncio),
    CONSTRAINT UNIQUE KEY UK_anuncios_casero_inmueble (usr_casero_uuid, inmueble_uuid),
    CONSTRAINT FK_anuncios_inmuebles FOREIGN KEY (inmueble_uuid) REFERENCES inmuebles(inmueble_uuid) 
        ON DELETE SET NULL,
    CONSTRAINT FK_anuncios_usr_casero_uuid FOREIGN KEY (usr_casero_uuid) REFERENCES inmuebles(usr_casero_uuid)
        ON DELETE SET NULL,
    CONSTRAINT PK_anuncios PRIMARY KEY (anuncio_uuid)
);


CREATE TABLE reservas (
    reserva_uuid VARCHAR(64) NOT NULL,
    id_reserva INT UNSIGNED NOT NULL AUTO_INCREMENT,
    usr_casero_uuid VARCHAR(64),
    usr_inquilino_uuid VARCHAR(64),
    inmueble_uuid VARCHAR(64),
    anuncio_uuid VARCHAR(64),
    fecha_reserva DATETIME NOT NULL,
    fecha_inicio DATE NOT NULL,
    fecha_fin DATE NOT NULL,
    precio_reserva INT UNSIGNED NOT NULL DEFAULT 0,
    estado_reserva ENUM('PENDIENTE','ACEPTADA','RECHAZADO','ALQUILER','FINALIZADA') NOT NULL DEFAULT 'PENDIENTE',
    tipo_pago_reserva ENUM('MENSUAL','SEMANAL','DIARIO','OTRO') NOT NULL DEFAULT 'MENSUAL',


    CONSTRAINT UNIQUE INDEX UI_reservas (id_reserva),
    CONSTRAINT FK_reservas_inmuebles_uuid FOREIGN KEY (inmueble_uuid) REFERENCES inmuebles(inmueble_uuid)
        ON DELETE SET NULL,
    CONSTRAINT FK_reservas_anuncio_uuid FOREIGN KEY (anuncio_uuid) REFERENCES anuncios(anuncio_uuid)
        ON DELETE SET NULL,
    CONSTRAINT FK_reservas_usr_casero_uuid FOREIGN KEY (usr_casero_uuid) REFERENCES inmuebles(usr_casero_uuid)
        ON DELETE SET NULL,
    CONSTRAINT FK_reservas_usr_inquilino_uuid FOREIGN KEY (usr_inquilino_uuid) REFERENCES usuarios(user_uuid)
        ON DELETE SET NULL,
    CONSTRAINT PK_reservas PRIMARY KEY(reserva_uuid)
);



CREATE TABLE resenas (
    resena_uuid VARCHAR(64) NOT NULL,
    id_resena INT UNSIGNED NOT NULL AUTO_INCREMENT,
    reserva_uuid VARCHAR(64) NOT NULL,
    autor_uuid VARCHAR(64) NOT NULL,
    rol ENUM('INQUILINO','CASERO', 'INQUILINO/CASERO', 'ADMIN') NOT NULL DEFAULT 'INQUILINO',
    objetivo ENUM('INQUILINO','CASERO', 'INMUEBLE') NOT NULL DEFAULT 'INMUEBLE',
    inmueble_uuid VARCHAR(64),
    usr_casero_uuid VARCHAR(64),
    usr_inquilino_uuid VARCHAR(64),
    puntuacion INT NOT NULL DEFAULT 0,
    contenido TEXT,

    INDEX IDX_autor_uuid(autor_uuid),
    CONSTRAINT UNIQUE INDEX UI_resenas (id_resena),
    CONSTRAINT UNIQUE KEY UK_reserva_autor_uuid (reserva_uuid, autor_uuid),
    CONSTRAINT FK_resenas_reserva_uuid FOREIGN KEY (reserva_uuid) REFERENCES reservas(reserva_uuid)
        ON DELETE CASCADE,
    CONSTRAINT FK_resenas_inmueble FOREIGN KEY (inmueble_uuid) REFERENCES inmuebles(inmueble_uuid)
        ON DELETE SET NULL,
    CONSTRAINT FK_resenas_usr_uuid_casero FOREIGN KEY (usr_casero_uuid) REFERENCES inmuebles(usr_casero_uuid)
        ON DELETE SET NULL,
    CONSTRAINT FK_resenas_usr_usuario FOREIGN KEY (usr_inquilino_uuid) REFERENCES usuarios(user_uuid)
        ON DELETE SET NULL,
    CONSTRAINT PK_resenas PRIMARY KEY(resena_uuid, reserva_uuid, autor_uuid)
);


CREATE TABLE img_resenas (
    img_resenas_uuid VARCHAR(64) NOT NULL,
    id_img_resenas INT UNSIGNED NOT NULL AUTO_INCREMENT,
    resena_uuid VARCHAR(64),
    autor_uuid VARCHAR(64),
    img_resena VARCHAR(128),

    CONSTRAINT UNIQUE INDEX UI_img_resenas (id_img_resenas),
    CONSTRAINT FK_img_resenas_resena_uuid FOREIGN KEY (resena_uuid) REFERENCES resenas(resena_uuid)
        ON DELETE CASCADE,
    CONSTRAINT FK_img_resenas_autor_uuid FOREIGN KEY (autor_uuid) REFERENCES resenas(autor_uuid)
        ON DELETE SET NULL,
    CONSTRAINT PK_img_resenas PRIMARY KEY (img_resenas_uuid)
);
