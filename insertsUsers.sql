--Al crear así los usuarios las contraseñas son inválidas y no se puede logear, para poder usar estos usuarios hay que hacer un update de los mismos desde el postman o crearlos en el postman

SET GLOBAL SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

INSERT INTO usuarios(id_usuario, user_uuid, username, password, email, tipo)
    VALUES(0,'cc77e9e5-4677-4ab6-ace9-902ef8e2e07d','admin','admin1234','asdf@asdf.asd','ADMIN');
INSERT INTO usuarios(id_usuario, user_uuid, username, password, email, tipo)
    VALUES(null,'fd0f280d-12f4-4561-96b8-8a9850612517','pepe','1234','asdf1@asdf.asd','INQUILINO');
INSERT INTO usuarios(id_usuario, user_uuid, username, password, email, tipo)
    VALUES(null,'d0f3bb42-fa87-4e3a-89d3-eb608eebc26b','hermeregildo','1234','asdf2@asdf.asd','CASERO');
INSERT INTO usuarios(id_usuario, user_uuid, username, password, email, tipo)
    VALUES(null,'3983bb27-f996-48a5-9cff-22c14ce6421f','johnDoe','1234','asdf3@asdf.asd','INQUILINO/CASERO');
INSERT INTO usuarios(id_usuario, user_uuid, username, password, email, tipo)
    VALUES(null,'b0cd1ee7-3979-4877-89a7-19dd4585335d','silicio','1234','asdf4@asdf.asd','INQUILINO');
INSERT INTO usuarios(id_usuario, user_uuid, username, password, email, tipo)
    VALUES(null,'a32f5c15-f7fd-44c8-9476-efcbf26a628c','cadmio','1234','asdf5@asdf.asd','INQUILINO');

SELECT * FROM usuarios;
