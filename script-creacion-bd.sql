DROP DATABASE inmocasa; 
CREATE DATABASE inmocasa;
USE inmocasa;

SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE usuarios (
	pk_codigo_usuario VARCHAR (20) PRIMARY KEY,
    nombre_usuario VARCHAR (25) NOT NULL UNIQUE,
    password VARCHAR(120) NOT NULL,
    tipo_codigo_usuario ENUM ('dni','pasaporte','nie') NOT NULL,
    email VARCHAR (120) NOT NULL,
    nombre VARCHAR (120) NOT NULL,
    bio VARCHAR(250),
    foto VARCHAR(150),
    es_casero BOOLEAN NOT NULL,
    es_inquilino BOOLEAN NOT NULL,
    telefono VARCHAR (25) NOT NULL
);

CREATE TABLE alertas_usuarios(
	pk_alertas INT AUTO_INCREMENT PRIMARY KEY,
    fk_id_usuario VARCHAR(20),
    FOREIGN KEY (fk_id_usuario) REFERENCES usuarios(pk_codigo_usuario),
    fK_id_inmueble INT UNSIGNED,
    FOREIGN KEY (fk_id_inmueble) REFERENCES inmuebles(pk),
    fecha_alerta_dispo DATE NOT NULL,
    alerta_nuevos BOOLEAN,
    precio_min FLOAT,
    precio_max FLOAT,
    num_habitaciones TINYINT NOT NULL,
    num_banos TINYINT NOT NULL,
    tipo_inmueble ENUM('piso','chalet','adosado','estudio') NOT NULL,
    ascensor BOOLEAN,
    garage BOOLEAN,
    amueblado BOOLEAN,
    calefaccion BOOLEAN
    
);

CREATE TABLE resenas_usuarios(
	pk INT UNSIGNED AUTO_INCREMENT PRIMARY KEY ,
    descripcion VARCHAR (500),
    fecha DATE NOT NULL,
    calificacion TINYINT NOT NULL
    
    
);
CREATE TABLE fotos_res_users(
	pk INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    foto VARCHAR (120) NOT NULL,
    fk_resenas_usuarios INT  UNSIGNED,
    FOREIGN KEY (fk_resenas_usuarios) REFERENCES resenas_usuarios(pk)
);


CREATE TABLE resenas_propietarios(
	pk INT UNSIGNED AUTO_INCREMENT PRIMARY KEY ,
    descripcion VARCHAR (500),
    fecha DATE NOT NULL,
    calificacion TINYINT NOT NULL
);

CREATE TABLE resenas_inmuebles(
	pk INT UNSIGNED AUTO_INCREMENT PRIMARY KEY ,
    descripcion VARCHAR (500),
    fecha DATE NOT NULL,
    calificacion TINYINT NOT NULL
    
    
);
CREATE TABLE fotos_res_inmuebles(
	pk INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    foto VARCHAR (120) NOT NULL,
    fk_resenas_inmuebles INT  UNSIGNED,
    FOREIGN KEY (fk_resenas_inmuebles) REFERENCES resenas_inmuebles(pk)
);



CREATE TABLE inmuebles (
	pk INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    fecha_alta DATE NOT NULL,
    precio FLOAT NOT NULL,
    localidad VARCHAR(50) NOT NULL,
    provincia VARCHAR(50) NOT NULL,
    tipo_via ENUM ('Calle','Avenida','Carretera','Camino','Parroquia','Lugar') NOT NULL,
    nombre_via VARCHAR (80) NOT NULL,
    numero TINYINT,
    planta VARCHAR(10),
    escalera_puerta VARCHAR(50),
    num_habitaciones TINYINT NOT NULL,
    num_banos TINYINT NOT NULL,
    fecha_disponibilidad DATE,
    disponible BOOLEAN NOT NULL,
    m2 TINYINT NOT NULL,
    tipo_inmueble ENUM('piso','chalet','adosado','estudio') NOT NULL,
    ascensor BOOLEAN,
    garage BOOLEAN,
    amueblado BOOLEAN,
    calefaccion BOOLEAN,
    trastero BOOLEAN,
    jardin BOOLEAN,
    notas VARCHAR (500),
    visible BOOLEAN NOT NULL,
    fk_usuarios VARCHAR (20),
    FOREIGN KEY (fk_usuarios) REFERENCES usuarios(pk_codigo_usuario)
);
CREATE TABLE  fotos_inmuebles(
	pk INT UNSIGNED NOT NULL,
    foto VARCHAR(150) NOT NULL,
    fk_inmuebles INT UNSIGNED,
    FOREIGN KEY (fk_inmuebles) REFERENCES inmuebles(pk)
);

CREATE TABLE alquileres (
	pk INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    fecha_inicio DATE NOT NULL,
    fecha_fin DATE NOT NULL,
    precio FLOAT NOT NULL,
    estado_reserva ENUM ('sin reserva','solicitada','confirmada','rechazada','finalizada') NOT NULL,
    fk_resenas_propietarios INT UNSIGNED,
    FOREIGN KEY (fk_resenas_propietarios) REFERENCES resenas_propietarios(pk),
    fk_resenas_usuarios INT UNSIGNED,
    FOREIGN KEY (fk_resenas_usuarios) REFERENCES resenas_usuarios(pk),
    fk_resenas_inmuebles INT  UNSIGNED,
    FOREIGN KEY (fk_resenas_inmuebles) REFERENCES resenas_inmuebles(pk),
    fk_inmuebles INT UNSIGNED,
    FOREIGN KEY (fk_inmuebles) REFERENCES inmuebles(pk),
    fk_usuarios VARCHAR (20),
    FOREIGN KEY (fk_usuarios) REFERENCES usuarios(pk_codigo_usuario)
);


SET FOREIGN_KEY_CHECKS = 1;